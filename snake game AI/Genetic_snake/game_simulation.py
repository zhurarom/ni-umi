import random

import time
import numpy as np
import pygame

from copy import copy

DIRECTIONS = {'LEFT': 0,
              'UP': 1,
              'RIGHT': 2,
              'DOWN': 3}


# Class which represent a snake
class SnakeGame:
    def __init__(self, neural_network, size=(20, 20), max_steps=3000):
        # Start configurations
        self.size = size
        self.max_steps = max_steps
        self.neural_network = neural_network
        self.fitness = 0

        # Configuration of window
        self.window_width = self.window_height = 880
        self.step = self.window_height / size[0]

        # Coordinates of food
        self.food_position = [random.randint(1, self.size[0] - 1), random.randint(1, self.size[1] - 1)]

        # Coordinates of snake body
        self.snake = [[4, 10], [4, 11], [4, 12]]

        # Coordinates of snake head
        self.head = [4, 10]

        # Start direction of a snake
        self.direction = DIRECTIONS['UP']

    # check for collision with self or the wall and end the game + penalise
    # penalise if it crashes with itself
    def penalization_for_crashing_yourself(self, fitness):
        if self.check_if_blocked():
            fitness -= 50
            return True
        return False

    # Check for eaten food, then move and extend the snake + generate new food
    def is_eaten(self, fitness):
        if self.check_if_food_eaten():
            self.move_snake(False)
            self.food_position = self.generate_new_food()
            fitness += 10
        else:
            self.move_snake(True)
        return fitness

    # Main loop for a game(pygame)
    def play_game(self, gui=False):
        fitness = 0.
        if gui:
            pygame.init()
            pygame.font.init()
            self.myfont = pygame.font.SysFont('Comic Sans MS', 40)
            self.window = pygame.display.set_mode((self.window_width, self.window_height))
            pygame.display.set_caption('SnakeAI')
            self.working = True
            self.snake_body = pygame.image.load("./Genetic_snake/assets/snake.png").convert()
            self.apple = pygame.image.load("./Genetic_snake/assets/apple.png").convert()

        # while not self.max_steps:
        for _ in range(self.max_steps):
            # Pygame events
            if gui:
                pygame.event.pump()
                keys = pygame.key.get_pressed()
                if keys[pygame.K_q]:
                    break

            # Check if we are blocked
            front_blocked, left_blocked, right_blocked = self.blocked_directions()

            # Calculate direction of a snake and apple
            apple_direction_vector, snake_direction_vector = self.direction_vectors()

            # Input to NN, to take the next turn
            turn = self.neural_network.predict_snake_direction(np.array([front_blocked,
                                                                         left_blocked, right_blocked,
                                                                         apple_direction_vector[0],
                                                                         apple_direction_vector[1],
                                                                         snake_direction_vector[0],
                                                                         snake_direction_vector[1]]))

            # Turn the snake
            self.change_direction(turn)

            if self.penalization_for_crashing_yourself(fitness):
                break

            fitness = self.is_eaten(fitness)

            # check if we moved towards the food or away -> reward or penalize
            if self.moved_toward_food():
                fitness += 1
            else:
                fitness -= 1.5

            # Gui representation
            if gui:
                self.print_background()
                self.draw_snake(self.window, self.snake_body)
                self.draw_apple(self.window, self.apple)
                textsurface = self.myfont.render(
                    'Score: ' + str(len(self.snake) - 2) + " | Press q to exit this generation",
                    True, (255, 255, 255))
                self.window.blit(textsurface, (3, 3))
                pygame.display.flip()
                time.sleep(50.0 / 1000.0)

        pygame.quit()
        return fitness

    # Check for self eating
    def kill_self(self):
        head_of_snake = self.snake[0]
        if head_of_snake in self.snake[1:]:
            return True
        else:
            return False

    # Check for killing by walls
    def killed_by_wall(self):
        if self.head[0] >= self.window_height or self.head[0] <= 0 or \
                self.head[1] >= self.window_width or self.head[1] <= 0:
            return True
        else:
            return False

    # Check is it blocked in a left?
    def blocked_left(self):
        self.change_direction(-1)
        if self.check_if_blocked():
            left_blocked = 1
        else:
            left_blocked = 0
        self.change_direction(1)
        return left_blocked

    # Check is it blocked in a right?
    def blocked_right(self):
        self.change_direction(1)
        if self.check_if_blocked():
            right_blocked = 1
        else:
            right_blocked = 0
        self.change_direction(-1)
        return right_blocked

    # Check for blocked direction
    def blocked_directions(self):
        if self.check_if_blocked():
            front_blocked = 1
        else:
            front_blocked = 0

        return front_blocked, self.blocked_left(), self.blocked_right()

    # Calculate direction of movement
    def direction_vectors(self):
        snake_direction_vector = np.array(self.snake[1]) - np.array(self.snake[0])
        apple_direction_vector = np.array(self.food_position) - np.array(self.snake[0])

        norm_of_apple_direction_vector = np.linalg.norm(apple_direction_vector)
        norm_of_snake_direction_vector = np.linalg.norm(snake_direction_vector)

        if norm_of_apple_direction_vector == 0:
            norm_of_apple_direction_vector = 10
        if norm_of_snake_direction_vector == 0:
            norm_of_snake_direction_vector = 10

        apple_direction_vector_normalized = apple_direction_vector / norm_of_apple_direction_vector
        snake_direction_vector_normalized = snake_direction_vector / norm_of_snake_direction_vector

        return apple_direction_vector_normalized, snake_direction_vector_normalized

    # Check if the snake moved towards food
    def moved_toward_food(self):
        return abs(self.snake[0][0] - self.food_position[0]) + abs(self.snake[0][1] - self.food_position[1]) \
               < \
               abs(self.snake[1][0] - self.food_position[0]) + abs(self.snake[1][1] - self.food_position[1])

    # Check for blocked directions
    def check_if_blocked(self):
        return ((self.direction == DIRECTIONS['LEFT'] and self.snake[0][0] == 0) or
                (self.direction == DIRECTIONS['UP'] and self.snake[0][1] == 0) or
                (self.direction == DIRECTIONS['RIGHT'] and self.snake[0][0] == self.size[0] - 1) or
                (self.direction == DIRECTIONS['DOWN'] and self.snake[0][1] == self.size[1] - 1) or
                self.new_head_pos() in self.snake)

    # Generate a new food
    def generate_new_food(self):
        new_food = [random.randint(1, self.size[0] - 1), random.randint(1, self.size[1] - 1)]
        return new_food

    # Change direction: -1 - Left;
    #                    0 - Ahead;
    #                    1 - Right
    def change_direction(self, turn):
        if turn == 0:
            return
        elif turn == -1:
            if self.direction == 0:
                self.direction = DIRECTIONS['DOWN']
            else:
                self.direction -= 1
        elif turn == 1:
            if self.direction == DIRECTIONS['DOWN']:
                self.direction = 0
            else:
                self.direction += 1
        else:
            exit(1)

    # Insert the new snake at the beginning and delete at the end if no apple was eaten
    def move_snake(self, delete_end):
        self.snake.insert(0, self.new_head_pos())
        if delete_end:
            del self.snake[len(self.snake) - 1]

    # Calculate new position of snake`s head
    def new_head_pos(self):
        new_head = copy(self.snake[0])
        if self.direction == 0:
            new_head[0] -= 1
        elif self.direction == 1:
            new_head[1] -= 1
        elif self.direction == 2:
            new_head[0] += 1
        elif self.direction == 3:
            new_head[1] += 1
        return new_head

    # Check for eating food
    def check_if_food_eaten(self):
        return self.new_head_pos() == self.food_position

    # Check for different type of collisions
    def check_for_collision(self):
        return self.killed_by_wall() or self.kill_self()

    # Draw a snake
    def draw_snake(self, surface, image):
        for i in range(0, len(self.snake)):
            surface.blit(image, (self.snake[i][0] * self.step, self.snake[i][1] * self.step))

    # Draw an apple
    def draw_apple(self, surface, image):
        surface.blit(image, (self.food_position[0] * self.step, self.food_position[1] * self.step))

    # Print a texture of a background
    def print_background(self):
        background = pygame.image.load('./Genetic_snake/assets/grace.png').convert()
        for y in range(441):
            for x in range(441):
                self.window.blit(background, [x * 64, y * 64])
