import Genetic_snake.genetic_algorithm as ga

from Genetic_snake.game_simulation import SnakeGame
from Genetic_snake.genetic_algorithm import POPULATION_SIZE, ITERATIONS
from Genetic_snake.neural_network import NeuralNetwork


# def make_print(new_score, best_score, new_best_nn):
#     # Make print, if current result is better, than the last best result
#     if new_score > best_score:
#         best_score = new_score
#         best_nn = new_best_nn
#         SnakeGame(best_nn, max_steps=1000000).play_game(gui=True)


# Main iteration loop for calculation the best fitness through populations
def make_iteration_loop(population, best_score, best_nn):
    for i in range(ITERATIONS):
        # For each generation we will calculate fitness
        population_fitness_tuples = ga.fitness(population)

        # Make smart selection
        parents, new_score, new_best_nn = ga.selection(population_fitness_tuples, best_score)

        # Print only a snake with the best result
        if new_score > best_score:
            best_score = new_score
            best_nn = new_best_nn
            SnakeGame(best_nn, max_steps=1000000).play_game(gui=True)
        # SnakeGame(best_nn, max_steps=1000000).play_game(gui=True)

        print("START OF GENETIC ALGORITHM")
        best_of_generation_tup = sorted(population_fitness_tuples, key=lambda x: x[1], reverse=True)[0]

        # Make print to see our changing of populations
        print(
            f"Generation {i}: Best fitness for this generation is : {best_of_generation_tup[1]}, The best from all "
            f"generations: {best_score}")

        # Make crossover to get a new generation
        population = ga.crossover(parents, best_nn)


# Printing loop for log in terminal
def log_terminal(best_nn):
    while True:
        SnakeGame(best_nn, max_steps=1000000).play_game(gui=True)


# Make initialization of first population (random one)
def genetic_snake():
    population = [NeuralNetwork() for i in range(POPULATION_SIZE)]

    best_score = 0
    best_nn = None

    make_iteration_loop(population, best_score, best_nn)
    log_terminal(best_nn)


if __name__ == '__main__':
    genetic_snake()
