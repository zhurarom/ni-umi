import pygame

class Snake:
    def __init__(self):
        self.head = [45, 45]
        self.body = [[45, 45], [34, 45], [23, 45]]
        self.score = 0

    # Allow to move our snake depend on direction
    def move(self, control):
        if control.flag_direction == "RIGHT":
            self.head[0] += 11
        elif control.flag_direction == "LEFT":
            self.head[0] -= 11
        elif control.flag_direction == "UP":
            self.head[1] -= 11
        elif control.flag_direction == "DOWN":
            self.head[1] += 11

    # + at the start of list a head and delete the last part of a snake
    def animation(self):
        self.body.insert(0, list(self.head))
        self.body.pop()

    # Animation of snake while move
    def draw_snake(self, window):
        for segment in self.body:
            pygame.draw.rect(window, pygame.Color("Black"), pygame.Rect(segment[0], segment[1], 10, 10))

    # Check for end of window
    def end_of_screen(self):
        if self.head[0] >= 410 or self.head[0] <= 21 or \
                self.head[1] >= 410 or self.head[1] <= 23:
            return True
        else:
            return False

    def kill_self(self):
        head_of_snake = self.body[0]
        if head_of_snake in self.body[1:]:
            return True
        else:
            return False

    # Eat a food
    def eat_food(self, food, gui):
        if self.head[0] == food.food_position[0] and self.head[1] == food.food_position[1]:
            self.score += 1
            self.body.append(food.food_position)
            food.get_food_position(gui)
            gui.get_new_indicator()

    def check_for_barrier(self, gui):
        if self.head in gui.list_of_walls:
            self.body.pop()
            gui.indicator.pop()
        if self.head in self.body[1:]:
            self.body.pop()
            gui.indicator.pop()
