import random


# Class which represent a food
class Food:
    def __init__(self):
        self.food_position = []

    def get_food_position(self, gui):
        self.food_position = random.choice(gui.field)

    def draw_food(self, window, apple):
        window.blit(apple, (self.food_position[0], self.food_position[1]))
