from Classic_snake.control import *
from Classic_snake.snake import *
from Classic_snake.food import *
from Classic_snake.gui import *
import time

pygame.init()
window = pygame.display.set_mode((441, 441))
pygame.display.set_caption("SnakeAI")

# Control of a snake
control = Control()
snake = Snake()
gui = Gui()
food = Food()
gui.init_level()
food.get_food_position(gui)

# images
background = pygame.image.load('./Classic_snake/texture/grace.png')
background_2 = pygame.image.load('./Classic_snake/texture/grace_2.png')

apple = pygame.image.load('./Classic_snake/texture/apple.png')
stone = pygame.image.load('./Classic_snake/texture/stone.png')


def display_final_score(text, how_many):
    pygame.font.init()
    largeText = pygame.font.SysFont('Comic Sans MS', 35)
    TextSurf = largeText.render(text, True, (0, 0, 0))
    TextRect = TextSurf.get_rect()
    window.blit(TextSurf, (441 / 2 - (TextRect[2] / 2), how_many))
    pygame.display.update()


counter = 0


# Main loop for a game menu
def classic_snake():
    gui.check_win_lose()
    speed_of_snake = 0
    while control.flag_game:
        control.control()

        # For fill background with image
        for y in range(441):
            for x in range(441):
                window.blit(background, [x * 64, y * 64])

        # If game -> draw snake and food
        if gui.game == "GAME":
            snake.draw_snake(window)
            food.draw_food(window, apple)
        elif gui.game == "WIN":
            display_final_score(f'YOU WIN', 250)
            display_final_score(f'Final score is: {str(snake.score)}', 180)
            display_final_score("Press any button to exit...", 240)

        gui.print_indicator(window)
        gui.draw_level(window)

        # Animation of snake head
        if speed_of_snake % 10 == 0 and control.flag_pause and gui.game == "GAME":
            snake.move(control)
            snake.check_for_barrier(gui)
            snake.eat_food(food, gui)
            if snake.end_of_screen() or snake.kill_self():
                display_final_score(f'Final score is: {str(snake.score)}', 180)
                display_final_score("Press any button to exit...", 240)
                time.sleep(2)
                if pygame.event.get():
                    control.flag_game = False
            else:
                snake.animation()
        speed_of_snake += 50

        pygame.display.flip()


if __name__ == '__main__':
    classic_snake()
