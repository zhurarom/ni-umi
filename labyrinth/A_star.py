from preprocessing import start_preprocessing
from animation import make_animation
from to_graph import mazetograph
from preprocessing import print_center
from preprocessing import make_print
import heapq
import time

'''Create heuristic form start and finish points'''


def heuristic(start, finish):
    return abs(start[0] - finish[0]) + abs(start[1] - finish[1])


'''A star algorithm'''


def find_a_star_search(maze, start, finish, screen):
    priority_queue = []
    heapq.heappush(priority_queue, (0 + heuristic(start, finish), 0, [start], start))
    visited = set()
    step = {'U': (0, -1), 'D': (0, 1), 'R': (1, 0), 'L': (-1, 0)}
    '''Transforming of maze into graph'''
    graph = mazetograph(maze, step)
    while priority_queue:
        make_animation(maze, screen)

        _, g, path, current = heapq.heappop(priority_queue)

        if current == finish:
            return path, visited

        if current in visited:
            continue
        visited.add(current)

        if maze[current[1]][current[0]] != "S":
            maze[current[1]][current[0]] = "*"

        for neighbour in graph[current]:
            heapq.heappush(priority_queue, (g + 1 + heuristic(neighbour, finish), g + 1, path + [neighbour], neighbour))


'''
    This function we call from start.py. Here we start preprocessing and finding a solve of a maze 
'''


def a_star_maze(screen, file):
    maze, start, finish = start_preprocessing(file)
    path, visited = find_a_star_search(maze, start, finish, screen)

    '''Use "P" for final path'''
    for col, row in path:
        make_animation(maze, screen)
        if maze[row][col] != "S":
            maze[row][col] = "P"

    time.sleep(1)

    make_print(screen, visited, path)
