import BFS
import DFS
import Greedy_search
import A_star
import Dijkstra
import Random_search

import curses
import time

# Represent main menu
menu = ['BFS', 'DFS', 'A*', 'Greedy', 'Dijkstra', 'Random Search', 'Exit']

'''
    Allows to print main menu
'''


def print_menu(stdscr, selected_row_idx):
    stdscr.clear()
    h, w = stdscr.getmaxyx()
    for idx, row in enumerate(menu):
        x = w // 2 - len(row) // 2
        y = h // 2 - len(menu) // 2 + idx
        if idx == selected_row_idx:
            stdscr.attron(curses.color_pair(8))
            stdscr.addstr(y, x, row)
            stdscr.attroff(curses.color_pair(8))
        else:
            stdscr.addstr(y, x, row)
    stdscr.refresh()


'''
    Get coordinates of center
'''


def print_center(stdscr, text):
    stdscr.clear()
    h, w = stdscr.getmaxyx()
    x = w // 2 - len(text) // 2
    y = h // 2
    stdscr.addstr(y, x, text)
    stdscr.refresh()


'''
    Represent a main menu, start of a program 
'''


def main(stdscr):
    # back color

    curses.init_pair(7, curses.COLOR_BLACK, curses.COLOR_WHITE)
    stdscr.bkgd(' ', curses.color_pair(7))

    # turn off cursor blinking
    curses.curs_set(0)

    # color scheme for selected row
    curses.init_pair(8, curses.COLOR_BLACK, curses.COLOR_RED)

    # specify the current selected row
    current_row = 0

    print_menu(stdscr, current_row)

    while 1:

        # Name of file for maze solving
        file = open("./tests/test_3.txt", "r")

        key = stdscr.getch()

        if key == curses.KEY_UP and current_row > 0:
            current_row -= 1
        elif key == curses.KEY_DOWN and current_row < len(menu) - 1:
            current_row += 1
        elif key == curses.KEY_ENTER or key in [10, 13]:
            print_center(stdscr, "Loading a maze for '{}'".format(menu[current_row]))
            time.sleep(1)

            if current_row == len(menu) - 1:
                break
            elif current_row == 0:
                BFS.start_bfs(stdscr, file)
            elif current_row == 1:
                DFS.start_dfs(stdscr, file)
            elif current_row == 2:
                A_star.a_star_maze(stdscr, file)
            elif current_row == 3:
                Greedy_search.start_greedy(stdscr, file)
            elif current_row == 4:
                Dijkstra.dijkstra_search(stdscr, file)
            elif current_row == 5:
                Random_search.start_random_search(stdscr, file)

            stdscr.getch()

        print_menu(stdscr, current_row)


curses.wrapper(main)
