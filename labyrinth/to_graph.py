"""Transforming of maze into graph """


def mazetograph(maze, steps):
    rows = len(maze)
    cols = len(maze[0])  # width
    graph = {(j, i): [] for j in range(cols) for i in range(rows) if not maze[i][j] == '1'}

    '''Use steps for choosing a sequence for creating of neighbours '''
    for col, row in graph.keys():
        for col_step, row_step in steps.values():
            new_col, new_row = col + col_step, row + row_step
            if new_col >= cols or new_row >= rows or new_col < 0 or new_row < 0:
                continue
            elif maze[new_row][new_col] in ["0", "F"]:
                graph[(col, row)].append((new_col, new_row))

    return graph


def print_graph(maze):
    graph = mazetograph(maze)
    for key, value in graph.items():
        print(key, ' : ', value)
