import curses
import time

'''
    Make animation for window
'''


def make_animation(maze, screen):
    rows = len(maze)
    cols = len(maze[0])

    screen.clear()

    for row in range(rows):
        for col in range(cols):
            try:
                '''Because of sign in a maze, we use different color '''
                if maze[row][col] == "1":
                    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_RED)
                    screen.attron(curses.color_pair(1))
                    screen.addstr(row, col * 2, maze[row][col] + ' ')
                    screen.attroff(curses.color_pair(1))

                elif maze[row][col] == "0":
                    curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_WHITE)
                    screen.attron(curses.color_pair(2))
                    screen.addstr(row, col * 2, maze[row][col] + ' ')
                    screen.attroff(curses.color_pair(2))

                elif maze[row][col] == "S":
                    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_GREEN)
                    screen.attron(curses.color_pair(3))
                    screen.addstr(row, col * 2, maze[row][col] + ' ')
                    screen.attroff(curses.color_pair(3))

                elif maze[row][col] == "F":
                    curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_CYAN)
                    screen.attron(curses.color_pair(4))
                    screen.addstr(row, col * 2, maze[row][col] + ' ')
                    screen.attroff(curses.color_pair(4))

                elif maze[row][col] == "*":
                    curses.init_pair(5, curses.COLOR_YELLOW, curses.COLOR_YELLOW)
                    screen.attron(curses.color_pair(5))
                    screen.addstr(row, col * 2, maze[row][col] + ' ')
                    screen.attroff(curses.color_pair(5))

                elif maze[row][col] == "P":
                    curses.init_pair(9, curses.COLOR_BLUE, curses.COLOR_BLUE)
                    screen.attron(curses.color_pair(9))
                    screen.addstr(row, col * 2, maze[row][col] + ' ')
                    screen.attroff(curses.color_pair(9))

            except curses.error:
                pass

    screen.refresh()
    time.sleep(0.01)
