import math
from variables import *
import random


# Make random movement in 2d vector if wa don`t have a DNA
class DNA(object):
    def __init__(self, genes=None):
        self.array = []
        self.dna_chain = pg.math.Vector2()

        if genes:
            self.array = genes
        else:
            # Generate random movements if we have frames(max count of movements)
            for i in range(moveLimit):
                self.dna_chain.xy = random.random()*2-1, random.random()*2-1
                self.array.append(self.dna_chain.xy)

    # Select a partner from the gene pool: Crossover + mutation
    #   (1) Choose randomly a point from DNA(2d vector) chain
    #   (2) Make mix from 2 genes
    #   (3) Create a new array of motion
    #   (4) Create a new DNA from the new created genes

    def CrossOver(self, partner):
        # Point (3)
        newGenes = []
        # Point (1)
        middle = math.floor(random.randrange(len(self.array)))
        # Point (2) + (4)
        for i in range(len(self.array)):
            if i < middle:
                newGenes.append(partner.array[i])
            else:
                newGenes.append(self.array[i])

        return DNA(newGenes)
