import pygame as pg
import random

pg.init()

# Setting up the screen
SW, SH = 1280, 720
SC = pg.display.set_mode((SW,SH))
pg.display.set_caption("NoWayBack")

# We need this to control FPS
clock = pg.time.Clock()
FPS = 60

# Parametres
moveLimit = 400
boxCount = 1000
generationCount = 0

# Like a time
frameCount = 0
levelCount = 1
successCount = 0

avgFitness = 0
avgFitnessD = 0

lowestTime = 0
lowestTimeD = 0

aliveBoxCount = boxCount
successCountD = 0

levelColor = (255, 255, 255)

finished = False
walls = []

# For genetic algo, the best boxes have more place in a pool
genePool = []

