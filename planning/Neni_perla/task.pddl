
(define (problem happy) 
    (:domain happy)
    (:requirements :strips)

    (:objects 
        kapitan divka ;-person
        pristav reka les mesto hospoda akademie more majak ostrov ;- place
        drevo alkohol zlate_zrnko zlate_mince zlata_cihle kvetiny medvedi_kuze kokos kokaine perla;- mine_object
        fregat karavela clun ;- craft_object
        prsten ; - for happy
        mapa ;- change_object
        nameteny_stav opily_stav zavisly_stav; - alkohol
        porazene_piraty zoceleni slava pochybne_znamosti dobre_znamosti zaznam_v_rejstriku seznameni_s_paseraky;-non_material
        stastny ; - goal
    )

    (:init
        ;Connection of islands
        (connect_land les reka)
        (connect_land reka les)
        
        (connect_land reka pristav)
        (connect_land pristav reka)

        (connect_land pristav hospoda)
        (connect_land hospoda pristav)

        (connect_land pristav mesto)
        (connect_land mesto pristav)

        (connect_land akademie mesto)
        (connect_land mesto akademie)

        (connect_water pristav majak)
        (connect_water majak pristav)

        (connect_water pristav more)
        (connect_water more pristav)
        
        (connect_water majak more)
        (connect_water more majak)
        
        (connect_water more ostrov)
        (connect_water ostrov more)

        (place pristav)
       
    )

    (:goal (and
           (is stastny) )
    )
)